<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string|null $name Назва
 * @property float|null $weight Вага
 * @property float|null $price Ціна
 * @property int|null $category_id Категорія
 * @property int|null $main_image_id Головне Фото
 * @property string|null $description Подробиці
 * @property string|null $seo_title Title
 * @property string|null $seo_keywords Keywords
 * @property string|null $seo_description Description
 *
 * @property Categories $category
 * @property ProductImages $mainImage
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['weight', 'price'], 'number'],
            [['category_id', 'main_image_id'], 'integer'],
            [['description', 'seo_description'], 'string'],
            [['name', 'seo_title', 'seo_keywords'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['main_image_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductImages::className(), 'targetAttribute' => ['main_image_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'weight' => 'Вага',
            'price' => 'Ціна',
            'category_id' => 'Категорія',
            'main_image_id' => 'Головне Фото',
            'description' => 'Подробиці',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[MainImage]].
     *
     * @return ActiveQuery
     */
    public function getMainImage()
    {
        return $this->hasOne(ProductImages::className(), ['id' => 'main_image_id']);
    }
}

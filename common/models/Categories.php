<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string|null $image
 * @property string $short_description
 * @property string $full_description
 * @property int|null $show_filter Показати фільтр
 * @property int|null $create_at Дата створення
 * @property int|null $updated_at Дата оновлення
 * @property string|null $seo_title Title
 * @property string|null $seo_keywords Keywords
 * @property string|null $seo_description Description
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @var mixed|string|null
     */
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug', 'title', 'short_description', 'full_description'], 'required'],
            [['short_description', 'full_description', 'seo_description'], 'string'],
            [['show_filter', 'create_at', 'updated_at'], 'integer'],
            [['slug', 'title', 'image', 'seo_title', 'seo_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'title' => 'Заголовок',
            'image' => 'Малюнок',
            'short_description' => 'Короткий опис',
            'full_description' => 'Повний опис',
            'show_filter' => 'Показати фільтр',
            'create_at' => 'Дата створення',
            'updated_at' => 'Дата оновлення',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
        ];
    }
    public static function getCategoriesList(){
        return ArrayHelper::map(self::find()->all(),'id','title');
    }
    public static function getCategory($id){
        $category=self::findOne($id);
        return $category->title;
    }
}

<?php

namespace common\models;

/**
 * This is the model class for table "carousel".
 *
 * @property int $id
 * @property string|null $title Заголовок
 * @property string|null $image Малюнок
 * @property string|null $content Опис
 * @property int|null $is_active Показувати
 */
class Carousel extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carousel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['is_active'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'image' => 'Малюнок',
            'content' => 'Опис',
            'is_active' => 'Показувати',
        ];
    }
}

<?php

namespace common\models;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string|null $key Ключ
 * @property string|null $value Значення
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'key' => 'Ключ',
            'value' => 'Значення',
        ];
    }
    public static function getValueSetting ($key)
    {
        /* @var $param self */
        $param=self::find()->where(['key'=>$key])->one();
        return $param->value ?? 'Не знайдено';
    }
}

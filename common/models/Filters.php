<?php

namespace common\models;

/**
 * This is the model class for table "filters".
 *
 * @property int $id
 * @property int|null $type Тип
 * @property string|null $name Назва
 * @property int|null $is_active Активний
 * @property string|null $categories Категорії
 */
class Filters extends \yii\db\ActiveRecord
{
    public $categoriesList;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filters';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'is_active'], 'integer'],
            [['categories'], 'string'],
            [['categoriesList'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'name' => 'Назва',
            'is_active' => 'Активний',
            'categories' => 'Категорії',
            'categoriesList'=>'Категорії'
        ];
    }
}

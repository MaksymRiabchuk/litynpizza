<?php

namespace common\models;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string $slug Слимак
 * @property string $title Заголовок
 * @property string $content Текст
 * @property int|null $create_at Дата створення
 * @property int|null $updated_at Дата оновлення
 * @property string|null $seo_title Title
 * @property string|null $seo_keywords Keywords
 * @property string|null $seo_description Description
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug', 'title', 'content'], 'required'],
            [['content', 'seo_description'], 'string'],
            [['create_at', 'updated_at'], 'integer'],
            [['slug', 'title', 'seo_title', 'seo_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'title' => 'Заголовок',
            'content' => 'Текст',
            'create_at' => 'Дата створення',
            'updated_at' => 'Дата оновлення',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
        ];
    }
}

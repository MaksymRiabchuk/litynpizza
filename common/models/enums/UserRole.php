<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class UserRole extends BaseEnum
{
    const ADMIN = 0;
    const MANAGER = 1;
    const USER = 9;

    /**
     * @var array
     */
    public static $list = [
        self::ADMIN => 'Адміністратор',
        self::MANAGER => 'Менеджер',
        self::USER => 'Користувач',
    ];
}

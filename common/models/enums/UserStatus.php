<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class UserStatus extends BaseEnum
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    /**
     * @var array
     */
    public static $list = [
        self::STATUS_DELETED => 'Видалений',
        self::STATUS_INACTIVE => 'Заблокований',
        self::STATUS_ACTIVE => 'Активний',
    ];
}

<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class FiltersType extends BaseEnum
{
    const MAINFILTER = 1;
    const INGRADIENT = 2;

    /**
     * @var array
     */
    public static $list = [
        self::MAINFILTER => 'Загальний',
        self::INGRADIENT => 'Інградієнт',
    ];
}

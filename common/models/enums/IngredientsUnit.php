<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class IngredientsUnit extends BaseEnum
{
    const GR = 1;
    const KG = 2;
    const LITR = 3;

    /**
     * @var array
     */
    public static $list = [
        self::GR => 'Грами',
        self::KG => 'Кілограми',
        self::LITR => 'Літри',
    ];
}

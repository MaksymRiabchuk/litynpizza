<?php

namespace common\models\search;

use common\models\ProductsIngredients;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProductsSearch represents the model behind the search form of `common\models\Products`.
 */
class ProductIngredientsSearch extends ProductsIngredients
{
    public $ingredientPrice;
    public $ingredientUnit;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_edit','product_id','ingredient_id'], 'integer'],
            [['amount'], 'number'],
            [['ingredientPrice','ingredientUnit'],'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $product_id)
    {
        $query = ProductsIngredients::find()->where(['product_id' => $product_id]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'ingredient_id'=> $this->ingredient_id,
            'is_edit'=>$this->is_edit,
            'amount'=> $this->amount,
        ]);
        return $dataProvider;
    }
}

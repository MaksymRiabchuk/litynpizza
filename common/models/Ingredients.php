<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ingredients".
 *
 * @property int $id
 * @property string|null $name Назва
 * @property string|null $image Малюнок
 * @property int|null $unit Одиниця виміру
 * @property float|null $price Ціна
 */
class Ingredients extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit'], 'integer'],
            [['price'], 'number'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'image' => 'Малюнок',
            'unit' => 'Одиниця виміру',
            'price' => 'Ціна',
        ];
    }
    public static function getIngredientsById()
    {
        return ArrayHelper::map(self::find()->all(),'id','name');
    }
}

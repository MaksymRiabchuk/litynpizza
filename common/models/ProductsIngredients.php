<?php

namespace common\models;

/**
 * This is the model class for table "ingredients_products".
 *
 * @property int $product_id
 * @property int $ingredient_id
 * @property float|null $amount Назва
 * @property int|null $is_edit Малюнок
 *
 * @property Ingredients $ingredient
 * @property Products $product
 */
class ProductsIngredients extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products_ingredients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'ingredient_id'], 'required'],
            [['product_id', 'ingredient_id', 'is_edit'], 'integer'],
            [['amount'], 'number'],
            [['product_id', 'ingredient_id'], 'unique', 'targetAttribute' => ['product_id', 'ingredient_id']],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredients::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'ingredient_id' => 'Назва',
            'amount' => 'Кількість',
            'is_edit' => 'Змінювати',
        ];
    }

    /**
     * Gets query for [[Ingredient]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}

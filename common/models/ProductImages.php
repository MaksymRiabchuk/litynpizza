<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the model class for table "products_image".
 *
 * @property int $id
 * @property string|null $name Назва
 * @property string|null $file_name І`мя файлу 
 * @property int|null $is_main Чиє голвним
 * @property integer $product_id
 * @property Products[] $products
 */
class ProductImages extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_main','id','product_id'], 'integer'],
            [['name', 'file_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'file_name' => 'Зображення',
            'is_main' => 'Основне',
            'product_id'=>'Товар',
        ];
    }

    /**
     * Gets query for [[Products]].
     *
     * @return ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['main_image_id' => 'id']);
    }
}

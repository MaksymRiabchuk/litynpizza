<?php

/* @var $this yii\web\View */
/* @var $profile \common\models\User*/


use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Профіль';
?>
<div class="site-index">
    <div class="body-content">
        <?php
        $form = ActiveForm::begin(); ?>
        <div class="panel panel-primary">
            <div class="panel panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12"><?= $form->field($profile, 'avatarFile')->widget(FileInput::classname(), [
                                    'options' => ['accept' => 'image/*'],
                                    'pluginOptions' => [
                                        'initialPreview' => ($profile->isNewRecord || !$profile->avatar) ? false : $profile->avatar,
                                        'initialPreviewAsData' => true,
                                        'showPreview' => true,
                                        'showRemove' => false,
                                        'showUpload' => false
                                    ],
                                ]);
                                ?></div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6"><?= $form->field($profile,
                                    'username')->textInput(['maxlength' => true,'disabled'=>true]) ?></div>
                            <div class="col-md-6"><?= $form->field($profile,
                                    'email')->textInput(['maxlength' => true,'disabled'=>true]) ?></div>
                            <div class="col-md-6"><?= $form->field($profile,
                                    'firstname')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-md-6"><?= $form->field($profile,
                                    'lastname')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-md-6"><?= $form->field($profile,
                                    'password')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-md-6"><?= $form->field($profile,
                                    'phone')->widget(\yii\widgets\MaskedInput::class, [
                                    'mask' => '(999) 999-99-99',
                                ]) ?></div>
                            <div class="col-md-6"><?= $form->field($profile,
                                    'birthday')->textInput(['type' => 'date']) ?></div>
                            <div class="col-md-6"><?= $form->field($profile,
                                    'address')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-md-4"><?= $form->field($profile,
                                    'viber')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-md-4"><?= $form->field($profile,
                                    'telegram')->textInput(['maxlength' => true]) ?></div>
                            <div class="col-md-4" style="padding-top: 20px"><?= $form->field($profile,
                                    'send_action')->checkbox() ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-footer">
                <div class="form-group">
                    <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?php
        ActiveForm::end(); ?>
    </div>
</div>

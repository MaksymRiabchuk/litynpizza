<?php

/* @var $this yii\web\View */

use common\models\Settings;
use yii\bootstrap\Carousel;

$this->title = 'My Yii Application';
/* @var $sliders array */
/* @var $slider \common\models\Carousel */
$carousel = [];
foreach ($sliders as $slider) {
    $carousel[] = [
        'content' => \yii\helpers\Html::img($slider->image).'<h1>'.$slider->title.'</h1>'.'<p>'.$slider->content.'</p>',
        'options' => [],
    ];
}
?>
<style>
    .item {
        height: auto;
        padding-left: 100px;
        padding-top: 10px;
        padding-right: 100px;
    }
   .item img{
       max-height: 300px;
       width: auto;
       margin: auto;
    }
    #w0 {
        height: 600px;
        margin: 20px 0px;
    }
    .item h1 {
        text-align: center;
    }

</style>
<div class="site-index">
    <div class="body-content">
        <?=
        Carousel::widget([
            'items' => $carousel,
            'options' => ['class' => 'carousel slide', 'data-interval' => '12000'],
            'controls' => [
                '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
                '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
            ]
        ]);
        ?>
        <div class="row">
            <?= Settings::getValueSetting('main-page') ?>
        </div>
    </div>
</div>

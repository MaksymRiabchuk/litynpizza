<?php

/* @var $this yii\web\View */
/* @var $page Page*/

use common\models\Page;

$this->title = isset($page->title)? $page->title : 'Сторінку не знайдено'  ;
?>
<div class="site-index">
    <div class="body-content">
        <?= isset($page->content)? $page->content : 'Сторінку не знайдено'?>
    </div>
</div>

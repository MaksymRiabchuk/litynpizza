<?php

use common\models\Settings;
use yii\helpers\Html;

?>
<style>
    .footer_link {
        display: block ;
        padding-bottom: 15px;
        padding-top: 15px;
    }
</style>
<footer>
    <div class="row">
        <div class="col-md-4">
            <h3>
                Адреса:
            </h3>
            <p>
                Вінницька обл.
            </p>
            <p>
                смт. Літин
            </p>
            <p>
                вул. Віницька 10
            </p>
        </div>
        <div class="col-md-4">
            <h3>
                Контакти:
            </h3>
            <p>
                Телефон:<?= Settings::getValueSetting('phone')?>
            </p>
            <p>
                Telegram:<?= Settings::getValueSetting('telegram')?>
            </p>
            <p>
                E-mail:<?= Settings::getValueSetting('email')?>
            </p>
        </div>
        <div class="col-md-4">
              <?= Html::a('Про нас',['page','slug'=>'about'],['class'=>'footer_link'] ) ?>
              <?= Html::a('Конткти',['page','slug'=>'contact'],['class'=>'footer_link'] ) ?>
              <?= Html::a('Оплата та доставка',['page','slug'=>'pay'],['class'=>'footer_link'] ) ?>
        </div>
    </div>
</footer>

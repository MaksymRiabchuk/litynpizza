<?php

use common\models\Categories;
use yii\helpers\Html;

$menuList = Categories::find()->all();
?>
<style>
    .menu {
        width: 100%;
        display: table;
    }
    .menu ul {
        display: table-row;
    }
    .menu li {
        display: table-cell;
        background: #2767A0;
    }
    .menu ul li:hover, .menu a:hover {
        background: #666;
    }
    .menu li a {
        display: block;
        padding: 8px 15px;
        color: #fff;
        text-align: center;
    }
    .logo img {
        max-width: 1140px;
    }
</style>
<header>
    <div class="logo" style="position: relative">
        <?= \yii\helpers\Html::img('/image/header.png') ?>
        <div class="login">
            <?=Yii::$app->user->isGuest ?
                Html::a('<i class="glyphicon glyphicon-log-in"></i> Вхід',['/site/login'])
                :
                Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Вийти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm();
            ?>
        </div>
    </div>

    <div class="menu">
        <ul>
        <?php /* @var $item Categories */ ?>
        <?php foreach ($menuList as $item): ?>
                    <li><?= \yii\helpers\Html::a($item->title,['categories','slug'=>$item->slug])?></li>
        <?php endforeach;?>
        </ul>
    </div>
</header>
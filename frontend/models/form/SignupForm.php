<?php
namespace frontend\models\form;

use common\models\enums\UserRole;
use common\models\User;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $username;
    public $email;
    public $password;
    public $firstname;
    public $lastname;
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username','firstname','lastname','phone','email'], 'trim'],
            [['username','firstname','lastname','phone','email','password'], 'required'],
            ['username', 'unique', 'targetClass' => 'common\models\User', 'message' => 'Цей никнейм вже використовуєця.'],
            [['username','firstname','lastname','email','phone'], 'string', 'min' => 2, 'max' => 255],
            ['phone', 'unique', 'targetClass' => 'common\models\User', 'message' => 'Цей телефон вже використовуєця.'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'common\models\User', 'message' => 'Ця пошта вже використовуєця.'],
            ['password', 'string', 'min' => 6],
        ];
    }
    public function attributeLabels ()
    {
        return [
            'username'=>'Никнейм',
            'email'=>'Пошта',
            'password'=>'Пароль',
            'firstname'=>'Ім`я',
            'lastname'=>'Прізвище',
            'phone'=>'Телефон',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        if (!$this->validate()) {
            return null;
        }
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->firstname=$this->firstname;
        $user->lastname=$this->lastname;
        $user->phone=$this->phone;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->role=UserRole::USER;
        $user->validate();
        var_dump($user->errors);
        return $user->save() ? $user : null;
    }

}
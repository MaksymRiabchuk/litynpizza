<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php
    $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?></div>

                <div class="col-md-6"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
            </div>

            <div class="row">
                <div class="col-md-4"><?= $form->field($model, 'imageFile')->widget(FileInput::classname(),
                        [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => ($model->isNewRecord || !$model->image) ? false : $model->image,
                                'initialPreviewAsData' => true,
                                'showPreview' => true,
                                'showRemove' => false,
                                'showUpload' => false
                            ],
                        ]); ?></div>
                <div class="col-md-8"><?= $form->field($model, 'show_filter')->checkbox() ?>
                    <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'full_description')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?></div>

                <div class="col-md-6"><?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?></div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
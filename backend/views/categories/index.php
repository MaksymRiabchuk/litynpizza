<?php

use common\models\Categories;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категорії';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <p>
        <?= Html::a('Добавити категорію', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'slug',
            'title',
            [
                    'attribute'=>'image',
                    'format'=>'raw',
                    'filter'=>false,
                    'value'=> function ($model){
                        return (Html::img($model->image,['style'=>'max-height:100px']));
                    },
            ],
            'short_description:ntext',
            //'full_description:ntext',
            [
                'attribute' => 'show_filter',
                'filter' => [0 => 'Ні', 1 => 'Так'],
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data Categories */
                    return $data->show_filter == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                        '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                }
            ],
            //'create_at',
            //'updated_at',
            //'seo_title',
            //'seo_keywords',
            //'seo_description:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

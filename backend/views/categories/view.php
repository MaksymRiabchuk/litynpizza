<?php

use common\models\Categories;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Категорії', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="categories-view">


    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно хочете видалити?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'slug',
            'title',
            [
                'attribute'=>'image',
                'format'=>'raw',
                'filter'=>false,
                'value'=> function ($model){
                    return (Html::img($model->image,['style'=>'max-height:100px']));
                },
            ],
            'short_description:ntext',
            'full_description:ntext',
            [
                'attribute' => 'show_filter',
                'filter' => [0 => 'Ні', 1 => 'Так'],
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data Categories */
                    return $data->show_filter == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                        '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                }
            ],
            'create_at',
            'updated_at',
            'seo_title',
            'seo_keywords',
            'seo_description:ntext',
        ],
    ]) ?>

</div>

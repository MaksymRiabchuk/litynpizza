<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Статистика', 'icon' => 'file-code-o', 'url' => ['/statistic']],
                    [
                        'label' => 'Користувачі',
                        'icon' => 'dashboard',
                        'url' => ['/user'],
                        'visible' => Yii::$app->user->identity->role == \common\models\enums\UserRole::ADMIN
                    ],
                    ['label' => 'Сторінки', 'icon' => 'dashboard', 'url' => ['/page']],
                    ['label' => 'Інградієнти', 'icon' => 'dashboard', 'url' => ['/ingredients']],
                    ['label' => 'Товари', 'icon' => 'dashboard', 'url' => ['/products']],
                    ['label' => 'Замовлення', 'icon' => 'dashboard', 'url' => ['/orders']],
                    ['label' => 'Фільтри', 'icon' => 'dashboard', 'url' => ['/filters']],
                    ['label' => 'Параметри', 'icon' => 'dashboard', 'url' => ['/settings']],
                    ['label' => 'Категорії', 'icon' => 'dashboard', 'url' => ['/categories']],
                    ['label' => 'Слайдери', 'icon' => 'dashboard', 'url' => ['/carousel']],
                ],
            ]
        ) ?>

    </section>

</aside>

<?php

use common\models\Filters;

/* @var $this yii\web\View */
/* @var $model common\models\Filters */
/* @var $filters Filters */

$this->title = 'Редагувати фільтр: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список фільтрів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="filters-update">


    <?= $this->render('_form', [
        'model' => $model,
        'filters'=>$filters
    ]) ?>

</div>

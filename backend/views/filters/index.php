<?php

use backend\controllers\FiltersController;
use common\models\Filters;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FiltersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderCategories FiltersController */
/* @var $searchModelCategories FiltersController */
/* @var $dataProviderIngredients FiltersController */
/* @var $searchModelIngredients FiltersController */
$this->title = 'Список фільтрів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filters-index">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#categories" aria-controls="categories" role="tab" data-toggle="tab">Категорії</a>
        </li>
        <li role="presentation">
            <a href="#ingredients" aria-controls="ingredients" role="tab" data-toggle="tab">Інградієнти</a>
        </li>
    </ul>
    <p>
        <?= Html::a('Добавити фільтр', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="categories">
            <?= GridView::widget([
                'dataProvider' => $dataProviderIngredients,
                'filterModel' => $searchModelIngredients,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    [
                        'attribute' => 'is_active',
                        'filter' => [0 => 'Ні', 1 => 'Так'],
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data Filters */
                            return $data->is_active == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                                '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                        }
                    ],
                    [
                        'attribute' => 'categories',
                        'filter' => true,
                        'value' => function ($data) {
                            $array = json_decode($data->categories);
                            $s = ' ';
                            foreach ($array as $item) {
                                $s = $s . ' ' . \common\models\Categories::getCategory($item);
                            }
                            return $s;
                        }
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="ingredients">
            <?= GridView::widget([
                'dataProvider' => $dataProviderCategories,
                'filterModel' => $searchModelCategories,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    [
                        'attribute' => 'is_active',
                        'filter' => [0 => 'Ні', 1 => 'Так'],
                        'format' => 'raw',
                        'value' => function ($data) {
                            /* @var $data Filters */
                            return $data->is_active == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                                '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                        }
                    ],
                    [
                        'attribute' => 'categories',
                        'filter' => true,
                        'value' => function ($data) {
                            $array = json_decode($data->categories);
                            $s = ' ';
                            foreach ($array as $item) {
                                $s = $s . ' ' . \common\models\Categories::getCategory($item);
                            }
                            return $s;
                        }
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?></div>
    </div>
</div>

<?php

use common\models\Filters;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Filters */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список фільтрів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="filters-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно хочте видалити?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'name',
            [
                    'attribute'=> 'is_active',
                    'format'=> 'raw',
                    'value'=> function ($data){
                    /* @var $data Filters */
                        return $data->is_active == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                            '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                    }
            ],
            [
                    'attribute'=>'categories',
                    'filter'=>true,
                'value' => function ($data) {
                    $array = json_decode($data->categories);
                    $s=' ';
                    foreach ($array as $item){
                        $s=$s.' '.\common\models\Categories::getCategory($item);
                    }
                    return $s;
                }
            ],
        ],
    ]) ?>

</div>

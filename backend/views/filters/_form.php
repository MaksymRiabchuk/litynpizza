<?php

use common\models\enums\FiltersType;
use common\models\Filters;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Filters */
/* @var $form yii\widgets\ActiveForm */
/* @var $filters Filters */
?>

<div class="filters-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'type')->dropDownList(FiltersType::listData()) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'is_active')->checkbox() ?>
                </div>
                <div class="col-md-2">
                    <div class="panel panel-primary">
                        <div class="panel panel-heading">
                            Категорії
                        </div>
                        <div class="panel panel-body">
                            <?= $form->field($model, 'categoriesList')->checkboxList($filters, ['separator' => '</br>'])->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php $form = ActiveForm::end(); ?>
</div>

<?php

use common\models\Filters;

/* @var $this yii\web\View */
/* @var $model common\models\Filters */
/* @var $filters Filters*/

$this->title = 'Добавити фільтр';
$this->params['breadcrumbs'][] = ['label' => 'Список фільтрів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filters-create">


    <?= $this->render('_form', [
        'model' => $model,
        'filters'=> $filters,
    ]) ?>

</div>

<?php

use common\models\Carousel;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Carousel */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Carousels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="carousel-view">

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно хочете видалити?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute'=>'image',
                'format'=>'raw',
                'filter'=>false,
                'value'=> function ($model){
                    return (Html::img($model->image,['style'=>'max-height:100px']));
                },
            ],
            'content:ntext',
            [
                'attribute' => 'is_active',
                'filter' => [0 => 'Ні', 1 => 'Так'],
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data Carousel */
                    return $data->is_active == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                        '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                }
            ],
        ],
    ]) ?>

</div>

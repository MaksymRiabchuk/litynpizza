<?php

use dosamigos\tinymce\TinyMce;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Carousel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-lg-10"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-2"
                     style="padding-top: 30px"><?= $form->field($model, 'is_active')->checkbox() ?></div>
            </div>
            <div class="row">
                <div class="col-md-4"> <?= $form->field($model, 'imageFile')->widget(FileInput::classname(),
                        [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => ($model->isNewRecord || !$model->image) ? false : $model->image,
                                'initialPreviewAsData' => true,
                                'showPreview' => true,
                                'showRemove' => false,
                                'showUpload' => false
                            ],
                        ]); ?>
                </div>
                <div class="col-md-8"><?= $form->field($model, 'content')->widget(TinyMce::className(), [
                        'options' => ['rows' => 18],
                        'language' => 'uk',
                        'clientOptions' => [
                            'plugins' => [
                                'advlist autolink lists link charmap hr preview pagebreak',
                                'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                                'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                            ],
                            'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                            'external_filemanager_path' => '/plugins/responsivefilemanager/filemanager/',
                            'filemanager_title' => 'Responsive Filemanager',
                            'external_plugins' => [
                                'filemanager' => '/plugins/responsivefilemanager/filemanager/plugin.min.js',
                                'responsivefilemanager' => '/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                            ],
                        ]
                    ]); ?>
                </div>
            </div>
                <div class="row">
                    <div class="col-md-12">
                            <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

/* @var $this yii\web\View */
/* @var $model common\models\Carousel */

$this->title = 'Добавити слайдер';
$this->params['breadcrumbs'][] = ['label' => 'Слайдери', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

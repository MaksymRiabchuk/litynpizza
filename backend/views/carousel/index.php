<?php

use common\models\Carousel;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CarouselSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдери';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-index">


    <p>
        <?= Html::a('Добавити слайдер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute'=>'image',
                'format'=>'raw',
                'filter'=>false,
                'value'=> function ($model){
                    return (Html::img($model->image,['style'=>'max-height:100px']));
                },
            ],
            [
                    'attribute'=>'content',
                    'format'=>'raw'
            ],
            [
                'attribute' => 'is_active',
                'filter' => [0 => 'Ні', 1 => 'Так'],
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data Carousel */
                    return $data->is_active == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                        '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

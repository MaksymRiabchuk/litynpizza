<?php

/* @var $this yii\web\View */
/* @var $model common\models\Carousel */

$this->title = 'Редагувати слайдер: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Слайдери', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="carousel-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">
    <?php
    $form = ActiveForm::begin(); ?>
    <div class="panel panel-default">
        <div class="panel panel-body">


            <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="panel panel-footer">
            <div class="form-group">
                <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
    <?php
    ActiveForm::end(); ?>
</div>

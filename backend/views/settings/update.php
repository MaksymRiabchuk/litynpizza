<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */

$this->title = 'Редагування параметра: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Параматр', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="settings-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

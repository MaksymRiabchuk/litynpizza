<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Параметри';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <div class="panel panel-default">
        <div class="panel panel-heading">
            <p>
                <?= Html::a('Створити параметр', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
        <div class="panel panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'key',
                    'value',

                    ['class' => 'yii\grid\ActionColumn', 'header' => 'Дії'],
                ],
            ]); ?>
        </div>
    </div>
</div>

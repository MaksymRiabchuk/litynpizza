<?php

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = 'Редагування сторінки: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Сторінки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Зберегти';
?>
<div class="page-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

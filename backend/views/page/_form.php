<?php

use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php
    $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?></div>

                <div class="col-md-6"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'content')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6],
                        'language' => 'uk',
                        'clientOptions' => [
                            'plugins' => [
                                'advlist autolink lists link charmap hr preview pagebreak',
                                'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                                'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                            ],
                            'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                            'external_filemanager_path' => '/plugins/responsivefilemanager/filemanager/',
                            'filemanager_title' => 'Responsive Filemanager',
                            'external_plugins' => [
                                //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                                'filemanager' => '/plugins/responsivefilemanager/filemanager/plugin.min.js',
                                //Иконка/кнопка загрузки файла в панеле иснструментов.
                                'responsivefilemanager' => '/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                            ],
                        ]
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model,
                        'seo_title')->textInput(['maxlength' => true]) ?></div>

                <div class="col-md-6"><?= $form->field($model,
                        'seo_keywords')->textInput(['maxlength' => true]) ?></div>

                <div class="col-md-12"><?= $form->field($model, 'seo_description')->textarea(['rows' => 5]) ?></div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    ActiveForm::end(); ?>
</div>


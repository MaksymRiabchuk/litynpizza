<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статична сторінка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">


    <p>
        <?= Html::a('Добавити сторінки', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'slug',
            'title',
            [
               'attribute'=>'content',
                'format'=>'raw',
            ],
            //'create_at',
            //'updated_at',
            //'seo_title',
            //'seo_keywords',
            //'seo_description:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

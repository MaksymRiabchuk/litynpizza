<?php

use common\models\enums\IngredientsUnit;
use common\models\Ingredients;
use common\models\ProductsIngredients;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductImages */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model ProductsIngredients */
/* @var $product_id integer */
/* @var $id integer */
/* @var $ingredient_id integer */


$this->title = 'Список Інгадієнтів';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="products-index">
    <div class="panel panel-default">
        <ul class="nav nav-tabs" role="tablist">
            <li>
                <?= Html::a('Основні', ['/products/update', 'id' => $product_id]) ?>
            </li>
            <li>
                <?= Html::a('Зображення', ['/products/product-images', 'product_id' => $product_id]) ?>
            </li>
            <li class="active">
                <?= Html::a('Інградієнти', ['/products/product-ingredients', 'product_id' => $product_id]) ?>
            </li>
        </ul>
        <div class="panel panel-heading">
            <p>
                <?= Html::a('Добавити Інградієнт', ['product-ingredients-create', 'product_id' => $product_id], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
        <div class="panel panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'ingredient_id',
                        'filter' => Ingredients::getIngredientsById(),
                        'format' => 'raw',
                        'value' => function ($model) {
                            return ($model->ingredient->name);
                        }
                    ],
                    [
                        'label' => 'Зображення',
                        'format'=>'raw',
                        'value' => function ($model) {
                            return ($model->ingredient->image ? (Html::img($model->ingredient->image,['style'=>'max-height:50px'])) : Html::img('/images/no_image.jpeg',['style'=>'max-height:50px']));
                        }
                    ],
                    [
                        'attribute'=>'ingredientUnit',
                        'label' => 'Одиниця виміру',
                        'filter'=> IngredientsUnit::listData(),
                        'value' => function ($model) {
                            return (IngredientsUnit::getLabel($model->ingredient->unit));
                        }

                    ],
                    [
                        'attribute'=>'ingredientPrice',
                        'label'=> 'Ціна',
                        'value'=> function ($model){
                            return($model->ingredient->price.' грн.');
                        }
                    ],
                    'amount',
                    [
                        'attribute'=>'is_edit',
                        'format'=>'raw',
                        'value'=>function($model){
                            return $model->is_edit == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                                '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) use ($product_id) {

                                return Html::a('', ['products/product-ingredients-update', 'ingredient_id' => $model->ingredient->id, 'product_id' => $product_id], ['class' => 'glyphicon glyphicon-pencil']);

                            },
                            'delete' => function ($url, $model, $key) use ($product_id) {

                                return Html::a('', ['products/product-ingredients-delete', 'ingredient_id' => $model->ingredient->id, 'product_id' => $product_id], ['class' => 'glyphicon glyphicon-trash']);
                            },
                        ],
                    ]]]); ?>
        </div>
    </div>
</div>

<?php

use common\models\Categories;
use common\models\Products;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список товарів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="products-view">


    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно хочете видалити?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'weight',
                'value' => function ($model) {
                    /* @var $model Products */
                    return $model->weight . ' грам';
                }
            ],
            [
                'attribute' => 'price',
                'value' => function ($model) {
                    /* @var $model Products */
                    return $model->price . ' грн';
                }
            ],
            [
                'attribute' => 'category_id',
                'filter' => true,
                'value' => function ($data) {
                    /* @var $data Products */
                    return ($data->category_id ? Categories::getCategory($data->category_id) : 'У цього товару немає категорії');
                }
            ],
            [
                'attribute' => 'main_image_id',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model Products */
                    return (isset($model->mainImage->file_name) ? Html::img($model->mainImage->file_name, ['style' => 'max-height:50px']) : Html::img('/images/no_image.jpeg', ['style' => 'max-height:30px']));
                }
            ],
            [
                    'attribute'=>'description',
                    'format'=>'raw',
            ],
            'seo_title',
            'seo_keywords',
            'seo_description:ntext',
        ],
    ]) ?>

</div>

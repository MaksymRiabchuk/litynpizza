<?php

/* @var $this yii\web\View */
/* @var $model common\models\ProductImages */

use common\models\Ingredients;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Редагувати інградієнта';
$this->params['breadcrumbs'][] = ['label' => 'Список інградієнтів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-update">


    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-4" style="padding-top: 10px"><?= $form->field($model, 'ingredient_id')->dropDownList(Ingredients::getIngredientsById()) ?></div>
                <div class="col-md-4 col-md-offset-4" style="padding-top: 10px"><?= $form->field($model, 'amount')->textInput() ?></div>
                <div class="col-md-4 col-md-offset-4" style="padding-top: 10px"><?= $form->field($model, 'is_edit')->checkbox() ?></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
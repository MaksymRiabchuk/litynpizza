<?php

/* @var $this yii\web\View */
/* @var $model common\models\ProductImages */

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Редагувати зображення';
$this->params['breadcrumbs'][] = ['label' => 'Список зображень', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-update">


    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'imageFile')->widget(FileInput::classname(),
                        [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => ($model->isNewRecord || !$model->file_name) ? false : $model->file_name,
                                'initialPreviewAsData' => true,
                                'showPreview' => true,
                                'showRemove' => false,
                                'showUpload' => false
                            ],
                        ]); ?></div>
                <div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
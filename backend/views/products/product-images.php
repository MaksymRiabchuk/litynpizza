<?php

use common\models\ProductImages;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductImages */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model ProductImages */
/* @var $product_id integer */
/* @var $id integer */
/* @var $ingredient_id integer */


$this->title = 'Список зображень';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="products-index">
    <div class="panel panel-default">
        <ul class="nav nav-tabs" role="tablist">
            <li>
                <?= Html::a('Основні', ['/products/update', 'id' => $product_id]) ?>
            </li>
            <li class="active">
                <?= Html::a('Зображення', ['/products/product-images', 'product_id' => $product_id]) ?>
            </li>
            <li>
                <?= Html::a('Інградієнти', ['/products/product-ingredients', 'product_id' => $product_id]) ?>
            </li>
        </ul>
        <div class="panel panel-heading">

            <p>
                <?= Html::a('Добавити зображення', ['product-image-create', 'product_id' => $product_id], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
        <div class="panel panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    [
                        'attribute' => 'file_name',
                        'format' => 'raw',
                        'filter' => false,
                        'value' => function ($model) {
                            return (Html::img($model->file_name, ['style' => 'max-height:50px']));
                        }

                    ],
                    [
                        'attribute' => 'is_main',
                        'format' => 'raw',
                        'filter' => false,
                        'value' => function ($model) {
                            return (!$model->is_main ? Html::a('<i style="color: red" class="glyphicon glyphicon-remove"></i>', ['products/set-main-image', 'image_id' => $model->id, 'product_id' => $model->product_id]) : '<i style="color: green" class="glyphicon glyphicon-ok"></i>');
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) use ($product_id) {

                                return Html::a('', ['products/product-image-update', 'image_id' => $model->id, 'product_id' => $product_id], ['class' => 'glyphicon glyphicon-pencil']);

                            },
                            'delete' => function ($url, $model, $key) use ($product_id) {

                                return Html::a('', ['products/product-image-delete', 'image_id' => $model->id, 'product_id' => $product_id], ['class' => 'glyphicon glyphicon-trash']);
                            },
                        ],
                    ],
                ]]); ?>
        </div>
    </div>
</div>

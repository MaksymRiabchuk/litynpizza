<?php

/* @var $this yii\web\View */

/* @var $model common\models\Products */
/* @var $product_id integer */

use common\models\Categories;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Редагувати товар: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список товарів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="products-update">

    <ul class="nav nav-tabs" role="tablist">
        <li class="active">
            <a href="#products" aria-controls="products" role="tab">Основні</a>
        </li>
        <li>
            <?=Html::a('Зображення',['/products/product-images','product_id'=>$model->id])?>
        </li>
        <li>
            <?= Html::a('Інградієнти', ['/products/product-ingredients', 'product_id'=> $model->id]) ?>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="products">
            <?php $form = ActiveForm::begin(); ?>
            <div class="panel panel-primary">
                <div class="panel panel-body">
                    <div class="row">
                        <div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-6"><?= $form->field($model, 'category_id')->dropDownList(Categories::getCategoriesList()) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= isset($model->mainImage->file_name) ? Html::img('/uploads/products' . $model->mainImage->file_name) : Html::img('/images/no_image.jpeg') ?>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                                        'options' => ['rows' => 6],
                                        'language' => 'uk',
                                        'clientOptions' => [
                                            'plugins' => [
                                                'advlist autolink lists link charmap hr preview pagebreak',
                                                'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                                                'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                                            ],
                                            'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                                            'external_filemanager_path' => '/plugins/responsivefilemanager/filemanager/',
                                            'filemanager_title' => 'Responsive Filemanager',
                                            'external_plugins' => [
                                                //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                                                'filemanager' => '/plugins/responsivefilemanager/filemanager/plugin.min.js',
                                                //Иконка/кнопка загрузки файла в панеле иснструментов.
                                                'responsivefilemanager' => '/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                                            ],
                                        ]
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'seo_description')->textarea(); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

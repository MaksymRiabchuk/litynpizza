<?php

use common\models\Categories;
use common\models\Products;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список товарів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">
    <p>
        <?= Html::a('Добавити товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'weight',
                'value' => function ($model) {
                    /* @var $model Products */
                    return $model->weight . ' грам';
                }
            ],
            [
                'attribute' => 'price',
                'value' => function ($model) {
                    /* @var $model Products */
                    return $model->price . ' грн';
                }
            ],
            [
                'attribute' => 'category_id',
                'filter' => true,
                'value' => function ($data) {
                    /* @var $data Products */
                    return ($data->category_id ? Categories::getCategory($data->category_id) : 'У цього товару немає категорії');
                }
            ],
            [
                'attribute' => 'main_image_id',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model Products */
                    return (isset($model->mainImage->file_name) ? Html::img($model->mainImage->file_name, ['style' => 'max-height:50px']) : Html::img('/images/no_image.jpeg', ['style' => 'max-height:30px']));
                }
            ],
            [
                'attribute' => 'description',
                'format' => 'raw',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ]]); ?>
</div>

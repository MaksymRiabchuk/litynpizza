<?php

use common\models\enums\UserRole;
use common\models\enums\UserStatus;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Користувачі';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">


    <p>
        <?= Html::a('Добавити користувача', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            //'verification_token',
            'firstname',
            'lastname',
            //'password',
            'phone',
            //'birthday',
            //'avatar',
            //'address',
            //'viber',
            //'telegram',
            [
                'attribute' => 'send_action',
                'filter' => [0 => 'Ні', 1 => 'Так'],
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data User */
                    return $data->send_action == 0 ? '<i style="color: red" class="glyphicon glyphicon-remove"></i>' :
                        '<i style="color: green" class="glyphicon glyphicon-ok"></i>';
                }
            ],
            [
                'attribute' => 'role',
                'filter' => UserRole::listData(),
                'value' => function ($data) {
                    /* @var $data User */
                    return UserRole::getLabel($data->role);
                }
            ],
            [
                'attribute' => 'status',
                'filter' => UserStatus::listData(),
                'value' => function ($data) {
                    /* @var $data User */
                    return UserStatus::getLabel($data->status);
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y'],
            ],

            ['class' => 'yii\grid\ActionColumn', 'header' => 'Дії'],
        ],
    ]); ?>


</div>

<?php

use common\models\enums\UserRole;
use common\models\enums\UserStatus;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php
    $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'avatarFile')->widget(FileInput::classname(),
                                [
                                    'options' => ['accept' => 'image/*'],
                                    'pluginOptions' => [
                                        'initialPreview' => ($model->isNewRecord || !$model->avatar) ? false : $model->avatar,
                                        'initialPreviewAsData' => true,
                                        'showPreview' => true,
                                        'showRemove' => false,
                                        'showUpload' => false
                                    ],
                                ]); ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6"><?= $form->field($model,
                                'username')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'email')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'firstname')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'lastname')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'password')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'phone')->widget(\yii\widgets\MaskedInput::class, [
                                'mask' => '(999) 999-99-99',
                            ]) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'birthday')->textInput(['type' => 'date']) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'address')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'viber')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-6"><?= $form->field($model,
                                'telegram')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-md-4"><?= $form->field($model,
                                'role')->dropDownList(UserRole::listData()) ?></div>
                        <div class="col-md-4"><?= $form->field($model,
                                'status')->dropDownList(UserStatus::listData()) ?></div>
                        <div class="col-md-4" style="padding-top: 20px"><?= $form->field($model,
                                'send_action')->checkbox() ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-footer">
            <div class="form-group">
                <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
    <?php
    ActiveForm::end(); ?>
</div>

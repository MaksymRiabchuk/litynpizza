<?php

/* @var $this yii\web\View */
/* @var $model common\models\Ingredients */

$this->title = 'Добавити інградієнт';
$this->params['breadcrumbs'][] = ['label' => 'Список інградієнтів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredients-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

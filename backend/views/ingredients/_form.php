<?php

use common\models\enums\IngredientsUnit;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Ingredients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ingredients-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'imageFile')->widget(FileInput::classname(),
                        [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => ($model->isNewRecord || !$model->image) ? false : $model->image,
                                'initialPreviewAsData' => true,
                                'showPreview' => true,
                                'showRemove' => false,
                                'showUpload' => false
                            ],
                        ]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'unit')->dropDownList(IngredientsUnit::listData()) ?>
                    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>

<?php

/* @var $this yii\web\View */
/* @var $model common\models\Ingredients */

$this->title = 'Update Ingredients: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список інградієнтів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="ingredients-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

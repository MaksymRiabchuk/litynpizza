<?php

use common\models\Ingredients;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Ingredients */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список інградієнтів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ingredients-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно хочете видалити?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute'=>'image',
                'format'=>'raw',
                'filter'=>false,
                'value'=> function ($model){
                    return (Html::img($model->image,['style'=>'max-height:100px']));
                },
            ],
            'unit',
            [
                'attribute'=>'price',
                'value'=> function ($model){
                    /* @var $model Ingredients */
                    return $model->price.' грн';
                }
            ],
        ],
    ]) ?>

</div>

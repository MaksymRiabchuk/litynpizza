<?php

use common\models\enums\IngredientsUnit;
use common\models\Ingredients;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\IngredientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список інградієнтів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredients-index">


    <p>
        <?= Html::a('Добавити інградієнт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute'=>'image',
                'format'=>'raw',
                'filter'=>false,
                'value'=> function ($model){
                    return (Html::img($model->image,['style'=>'max-height:30px']));
                },
            ],
            [
                    'attribute'=>'unit',
                    'filter'=>IngredientsUnit::listData(),
            ],
            [
                    'attribute'=>'price',
                    'value'=> function ($model){
                        /* @var $model Ingredients */
                        return $model->price.' грн';
                    }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

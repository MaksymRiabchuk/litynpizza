<?php

namespace backend\controllers;

use common\models\Categories;
use common\models\enums\FiltersType;
use common\models\Filters;
use common\models\FiltersSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * FiltersController implements the CRUD actions for Filters model.
 */
class FiltersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Filters models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModelIngredients = new FiltersSearch(['type'=>FiltersType::INGRADIENT]);
        $dataProviderIngredients = $searchModelIngredients->search(Yii::$app->request->queryParams);

        $searchModelCategories = new FiltersSearch(['type'=>FiltersType::MAINFILTER]);
        $dataProviderCategories = $searchModelCategories->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModelIngredients' => $searchModelIngredients,
            'dataProviderIngredients' => $dataProviderIngredients,
            'searchModelCategories' => $searchModelCategories,
            'dataProviderCategories' => $dataProviderCategories,
        ]);

    }

    /**
     * Displays a single Filters model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Filters model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Filters();
        $filters=Categories::getCategoriesList();
        if ($model->load(Yii::$app->request->post())) {
            $model->categories=json_encode($model->categoriesList);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'filters'=>$filters
        ]);
    }

    /**
     * Updates an existing Filters model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->categoriesList=json_decode($model->categories);
        $filters=Categories::getCategoriesList();

        if ($model->load(Yii::$app->request->post())) {
            $model->categories=json_encode($model->categoriesList);
            $model->save();

            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'filters'=> $filters
        ]);
    }

    /**
     * Deletes an existing Filters model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Filters model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Filters the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Filters::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

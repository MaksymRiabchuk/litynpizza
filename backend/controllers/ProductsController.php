<?php

namespace backend\controllers;

use common\models\ProductImages;
use common\models\Products;
use common\models\ProductsIngredients;
use common\models\ProductsSearch;
use common\models\search\ProductImagesSearch;
use common\models\search\ProductIngredientsSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionProductImages($product_id){
        $searchModel = new ProductImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$product_id);
        return $this->render('product-images', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'product_id'=> $product_id,
        ]);
    }

    public function actionProductImageCreate($product_id){
        $model= new ProductImages();
        $model->product_id=$product_id;

        if ($model->load(Yii::$app->request->post())) {
            $model->save();

            $image = UploadedFile::getInstance($model, 'imageFile');
            if ($image) {
                $image->saveAs('uploads/products/' . $model->id . '.' . $image->getExtension());
                $model->file_name = '/uploads/products/' . $model->id . '.' . $image->getExtension();
                $model->save();
            }

            if ($model->errors) {
                var_dump($model->errors);
            } else {
                return $this->redirect(['product-images', 'product_id' => $product_id]);
            }
        }
        return $this->render('product-images-create', [
            'model' => $model,
            'product_id'=>$product_id,
        ]);
    }
    public function actionProductImageUpdate($image_id,$product_id){
        $model=ProductImages::find()->where(['id'=>$image_id])->one();
        $model->imageFile=$model->file_name;
        if ($model->load(Yii::$app->request->post())) {
            $model->save();

            $image = UploadedFile::getInstance($model, 'imageFile');
            if ($image) {
                $image->saveAs('uploads/products/' . $model->id . '.' . $image->getExtension());
                $model->file_name = '/uploads/products/' . $model->id . '.' . $image->getExtension();
                $model->save();
            }

            if ($model->errors) {
                var_dump($model->errors);
            } else {
                return $this->redirect(['product-images', 'product_id' => $product_id]);
            }
        }
        return $this->render('product-images-update', [
            'model' => $model,
            'product_id'=>$product_id,
        ]);
    }
    public function actionProductImageDelete($image_id,$product_id)
    {
        $model=ProductImages::find()->where(['id'=>$image_id])->one();
        $model->delete();

        return $this->redirect(['product-images','product_id'=>$product_id]);
    }
    public function  actionSetMainImage($image_id,$product_id){
        $main_id=ProductImages::find()->where(['is_main'=>1,'product_id'=>$product_id])->one();
        if ($main_id){
            $main_id->is_main=0;
            $main_id->save();
        }
        $model=ProductImages::find()->where(['id'=>$image_id])->one();
        if ($model){
            $model->is_main=1;
            $model->save();
        }
        return $this->redirect(['product-images', 'product_id' => $product_id]);
    }
    public function actionProductIngredients($product_id){
        $searchModel = new ProductIngredientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$product_id);
        return $this->render('product-ingredients', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'product_id'=> $product_id,
        ]);
    }
    public function actionProductIngredientsCreate($product_id){
        $model= new ProductsIngredients();
        $model->product_id=$product_id;

        if ($model->load(Yii::$app->request->post())) {
            $model->save();

            $image = UploadedFile::getInstance($model, 'imageFile');
            if ($image) {
                $image->saveAs('uploads/products/' . $model->ingredient_id . '.' . $image->getExtension());
                $model->image = '/uploads/products/' . $model->ingredient_id . '.' . $image->getExtension();
                $model->save();
            }

            if ($model->errors) {
                var_dump($model->errors);
            } else {
                return $this->redirect(['product-ingredients', 'product_id' => $product_id]);
            }
        }
        return $this->render('product-ingredients-create', [
            'model' => $model,
            'product_id'=>$product_id,
        ]);
    }
    public function actionProductIngredientsUpdate($ingredient_id,$product_id){
        $model=ProductsIngredients::find()->where(['ingredient_id'=>$ingredient_id])->one();
        $model->imageFile=$model->ingredient->image;
        if ($model->load(Yii::$app->request->post())) {
            $model->save();

            $image = UploadedFile::getInstance($model, 'imageFile');
            if ($image) {
                $image->saveAs('uploads/ingredients/' . $model->id . '.' . $image->getExtension());
                $model->ingredient->image = '/uploads/ingredients/' . $model->id . '.' . $image->getExtension();
                $model->save();
            }

            if ($model->errors) {
                var_dump($model->errors);
            } else {
                return $this->redirect(['product-ingredients', 'product_id' => $product_id]);
            }
        }
        return $this->render('product-ingredients-update', [
            'model' => $model,
            'product_id'=>$product_id,
        ]);
    }
    public function actionProductIngredientsDelete($ingredient_id,$product_id)
    {
        $model=ProductsIngredients::find()->where(['ingredient_id'=>$ingredient_id,'product_id'=>$product_id])->one();
        $model->delete();
        return $this->redirect(['product-ingredients','product_id'=>$product_id]);
    }
}

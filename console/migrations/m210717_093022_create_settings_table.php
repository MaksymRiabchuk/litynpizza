<?php

use yii\db\Migration;

/**
 * Class m210717_093022_create_settings_table
 */
class m210717_093022_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}',[
            'id'=> $this->primaryKey(),
            'key'=> $this->string()->unique()->comment('Ключ'),
            'value'=> $this->string()->comment('Значення'),
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}

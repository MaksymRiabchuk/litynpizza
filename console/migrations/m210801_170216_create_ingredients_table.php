<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingredients}}`.
 */
class m210801_170216_create_ingredients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ingredients}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Назва'),
            'image' => $this->string(255)->comment('Малюнок'),
            'unit' => $this->smallInteger()->comment('Одиниця виміру'),
            'price' => $this->decimal(10,2)->comment('Ціна'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ingredients}}');
    }
}

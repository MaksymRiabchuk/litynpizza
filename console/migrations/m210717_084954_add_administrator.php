<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m210717_084954_add_administrator
 */
class m210717_084954_add_administrator extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $user= new User();
        $user->username='admin';
        $user->email='admin@gmail.com';
        $user->setPassword('admin');
        $user->status= User::STATUS_ACTIVE;
        $user->generateAuthKey();
        $user->save(false);
        var_dump($user->validate());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $user= User::find()->where(['username'=>'admin'])->one();
        $user->delete();
    }
}

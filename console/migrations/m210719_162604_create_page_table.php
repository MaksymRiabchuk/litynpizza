<?php

use yii\db\Migration;

/**
 * Class m210719_162604_create_page_table
 */
class m210719_162604_create_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%page}}',[
            'id'=> $this->primaryKey(),
            'slug'=> $this->string()->comment('Слимак')->notNull(),
            'title'=> $this->string()->comment('Заголовок')->notNull(),
            'content'=> $this->text()->comment('Текст')->notNull(),
            'create_at'=> $this->integer()->comment('Дата створення'),
            'updated_at'=> $this->integer()->comment('Дата оновлення'),
            'seo_title'=> $this->string()->comment('Title'),
            'seo_keywords'=> $this->string()->comment('Keywords'),
            'seo_description'=> $this->text()->comment('Description'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%page}}');
    }
}

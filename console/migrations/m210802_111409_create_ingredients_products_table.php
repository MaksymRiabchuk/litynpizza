<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingredients_products}}`.
 */
class m210802_111409_create_ingredients_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{products_ingredients}}', [
            'product_id' => $this->integer()->notNull(),
            'ingredient_id' => $this->integer()->notNull(),
            'amount' => $this->decimal(10, 3)->comment('Кількість'),
            'is_edit' => $this->boolean(),
        ]);
        $this->addPrimaryKey('PK-ingredients_products', '{{%products_ingredients%}}', ['product_id', 'ingredient_id']);
        $this->addForeignKey('FK-ingredients_products-product_id-products-id', '{{%products_ingredients%}}', 'product_id', '{{%products%}}', 'id');
        $this->addForeignKey('FK-ingredients_products-ingredient_id-ingredients-id', '{{%products_ingredients%}}', 'ingredient_id', '{{%ingredients%}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%products_ingredients}}');
    }
}

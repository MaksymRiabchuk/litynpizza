<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products_image}}`.
 */
class m210801_152310_create_products_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_images}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string()->comment('Назва'),
            'file_name'=> $this->string()->comment('І`мя файлу '),
            'is_main'=> $this->boolean()->comment('Чиє головним'),
            'product_id'=> $this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%products_image}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 */
class m210801_153953_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string()->comment('Назва'),
            'weight'=> $this->decimal(10,3)->comment('Вага'),
            'price'=> $this->decimal(10,2)->comment('Ціна'),
            'category_id'=> $this->integer()->comment('Категорія'),
            'main_image_id'=> $this->integer()->comment('Головне Фото'),
            'description'=> $this->text()->comment('Подробиці'),
            'seo_title'=> $this->string()->comment('Title'),
            'seo_keywords'=> $this->string()->comment('Keywords'),
            'seo_description'=> $this->text()->comment('Description'),
        ]);
        $this->addForeignKey('FK-products-category_id-categories-id','{{%products%}}','category_id','categories','id');
        $this->addForeignKey('FK-products-main_image_id-products_image-id','{{%products%}}','main_image_id','product_images','id');
        $this->addForeignKey('FK-product_images-product_id-products-id',
            '{{%product_images%}}','product_id','products','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%products}}');
    }
}

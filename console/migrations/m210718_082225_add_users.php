<?php

use yii\db\Migration;

/**
 * Class m210718_082225_add_users
 */
class m210718_082225_add_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'firstname', $this->string()->notNull());
        $this->addColumn('{{%user}}', 'lastname', $this->string()->notNull());
        $this->addColumn('{{%user}}', 'phone', $this->string()->notNull());
        $this->addColumn('{{%user}}', 'birthday', $this->date());
        $this->addColumn('{{%user}}', 'avatar', $this->string());
        $this->addColumn('{{%user}}', 'address', $this->string());
        $this->addColumn('{{%user}}', 'viber', $this->string());
        $this->addColumn('{{%user}}', 'telegram', $this->string());
        $this->addColumn('{{%user}}', 'send_action', $this->boolean()->defaultValue(true));
        $this->addColumn('{{%user}}', 'role', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'firstname');
        $this->dropColumn('{{%user}}', 'lastname');
        $this->dropColumn('{{%user}}', 'phone');
        $this->dropColumn('{{%user}}', 'birthday');
        $this->dropColumn('{{%user}}', 'avatar');
        $this->dropColumn('{{%user}}', 'address');
        $this->dropColumn('{{%user}}', 'viber');
        $this->dropColumn('{{%user}}', 'telegram');
        $this->dropColumn('{{%user}}', 'send_action');
        $this->dropColumn('{{%user}}', 'role');
    }
}

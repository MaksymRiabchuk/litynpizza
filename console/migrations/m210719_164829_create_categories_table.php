<?php

use yii\db\Migration;

/**
 * Class m210719_164829_create_categories_table
 */
class m210719_164829_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%categories}}',[
            'id'=> $this->primaryKey()->notNull(),
            'slug'=> $this->string()->notNull(),
            'title'=> $this->string()->notNull(),
            'image'=> $this->string(),
            'short_description'=> $this->text()->notNull(),
            'full_description'=> $this->text()->notNull(),
            'show_filter'=> $this->boolean()->comment('Показати фільтр'),
            'create_at'=> $this->integer()->comment('Дата створення'),
            'updated_at'=> $this->integer()->comment('Дата оновлення'),
            'seo_title'=> $this->string()->comment('Title'),
            'seo_keywords'=> $this->string()->comment('Keywords'),
            'seo_description'=> $this->text()->comment('Description'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%categories}}');
    }
}

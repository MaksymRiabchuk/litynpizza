<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%carousel}}`.
 */
class m210725_114154_create_carousel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%carousel}}', [
            'id' => $this->primaryKey(),
            'title'=> $this->string()->comment('Заголовок'),
            'image'=> $this->string()->comment('Малюнок'),
            'content'=> $this->text()->comment('Опис'),
            'is_active'=> $this->boolean()->comment('Показувати'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%carousel}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filters}}`.
 */
class m210720_102957_create_filters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%filters}}', [
            'id' => $this->primaryKey(),
            'type' =>$this->integer()->comment('Тип'),
            'name' =>$this->string()->comment('Назва'),
            'is_active' =>$this->boolean()->comment('Активний'),
            'categories'=> $this->text()->comment('Категорії'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%filters}}');
    }
}
